# Changelog
Todas as mudanças do projeto serão mantidas neste arquivo.

Formato baseado em: [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

Este projeto adere ao sistema de versionamento: [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2019-09-21
### Add
- Adicionado o sistema de criação do usuário.
