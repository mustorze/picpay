<?php

namespace App\Services;

use App\Domains\User\Models\Consumer;
use App\Domains\User\Models\Seller;

class AccountService
{
    /**
     * Check existence of account by ID
     *
     * @param int $id
     * @return bool
     */
    public function exists(int $id): bool
    {
        $consumer = Consumer::where('id', $id)
            ->exists();

        if (!$consumer) {
            $seller = Seller::where('id', $id)
                ->exists();
        }

        return ($consumer or ($seller ?? false));
    }
}
