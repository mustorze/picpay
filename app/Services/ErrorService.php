<?php

namespace App\Services;

use App\Exceptions\GenericException;
use App\Interfaces\ErrorInterface;
use Illuminate\Http\JsonResponse;

class ErrorService
{
    /**
     * Generate response from error
     *
     * @param ErrorInterface $error
     * @param int $code
     * @return JsonResponse
     */
    public function response(ErrorInterface $error, int $code = 422): JsonResponse
    {
        return response()->json([
            'code' => $error->code(),
            'message' => $error->message()
        ], $code);
    }

    /**
     * Throw an generic exception
     *
     * @param ErrorInterface $error
     * @param int $code
     * @throws GenericException
     */
    public function throw(ErrorInterface $error, int $code = 500): void
    {
        throw new GenericException($error, $code);
    }
}
