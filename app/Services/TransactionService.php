<?php

namespace App\Services;

use App\Domains\Transaction\Models\Transaction;
use App\Exceptions\GenericException;
use App\Http\Errors\UnauthorizedError;
use App\Interfaces\GatewayInterface;
use Throwable;

class TransactionService
{
    private $gateway;

    public function __construct(GatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * Make an account-to-account transaction through a gateway
     *
     * @param int $payerId
     * @param int $payeeId
     * @param $value
     * @return Transaction
     * @throws Throwable
     */
    public function transact(int $payerId, int $payeeId, $value): Transaction
    {
        $transactionResponse = $this->gateway
            ->transfer($payerId, $payeeId, $value);

        throw_unless($transactionResponse->status, new GenericException(new UnauthorizedError, 401));

        return Transaction::create([
            'payer_id' => $payerId,
            'payee_id' => $payeeId,
            'value' => $this->toInt($value)
        ]);
    }

    /**
     * Convert value to int
     *
     * @param $value
     * @return float|int
     */
    public function toInt($value): int
    {
        return $value * 100;
    }

    /**
     * Convert value to float
     *
     * @param $value
     * @return float|int
     */
    public function toFloat($value): float
    {
        return $value / 100;
    }
}
