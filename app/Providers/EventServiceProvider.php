<?php

namespace App\Providers;

use App\Domains\Transaction\Events\TransactionWasCreatedEvent;
use App\Domains\User\Events\ConsumerWasCreatedEvent;
use App\Domains\User\Events\Listeners\LogConsumerCreationListener;
use App\Domains\User\Events\Listeners\LogSellerCreationListener;
use App\Domains\User\Events\Listeners\LogTransactionCreationListener;
use App\Domains\User\Events\Listeners\LogUserCreationListener;
use App\Domains\User\Events\SellerWasCreatedEvent;
use App\Domains\User\Events\UserWasCreatedEvent;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserWasCreatedEvent::class => [
            LogUserCreationListener::class
        ],
        ConsumerWasCreatedEvent::class => [
            LogConsumerCreationListener::class
        ],
        SellerWasCreatedEvent::class => [
            LogSellerCreationListener::class
        ],
        TransactionWasCreatedEvent::class => [
            LogTransactionCreationListener::class
        ]
    ];
}
