<?php

namespace App\Providers;

use App\Domains\Transaction\Gateways\PaymentGateway;
use App\Domains\Transaction\Models\Transaction;
use App\Domains\Transaction\Models\TransactionObserver;
use App\Domains\User\Models\Consumer;
use App\Domains\User\Models\Seller;
use App\Domains\User\Models\User;
use App\Domains\User\Requests\ConsumerObserver;
use App\Domains\User\Requests\SellerObserver;
use App\Domains\User\Requests\UserObserver;
use App\Interfaces\GatewayInterface;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {
        Resource::withoutWrapping();

        $this->registerObservers();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GatewayInterface::class, PaymentGateway::class);
    }

    private function registerObservers()
    {
        User::observe(UserObserver::class);
        Consumer::observe(ConsumerObserver::class);
        Seller::observe(SellerObserver::class);
        Transaction::observe(TransactionObserver::class);
    }
}
