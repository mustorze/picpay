<?php

namespace App\Interfaces;

abstract class ActionBase implements ActionInterface
{
    /**
     * Perform checks before call action
     *
     * @return bool
     */
    public function before(): bool
    {
        return true;
    }

    /**
     * Treatment if check goes fail
     *
     * @return mixed
     */
    public function beforeError()
    {
    }

    /**
     * Perform the action
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->before()) {
            return $this->action();
        }

        return $this->beforeError();
    }
}
