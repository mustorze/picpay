<?php

namespace App\Interfaces;

use App\Domains\Transaction\Gateways\Responses\TransferResponse;

interface GatewayInterface
{
    /**
     * Make an account-to-account transaction
     *
     * @param int $payer
     * @param int $payee
     * @param $value
     * @return TransferResponse
     */
    public function transfer(int $payer, int $payee, $value): TransferResponse;

    /**
     * Check for balance in account
     *
     * @param int $accountId
     * @param $value
     * @return bool
     */
    public function haveBalance(int $accountId, $value): bool;
}
