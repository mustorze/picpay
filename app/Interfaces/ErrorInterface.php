<?php

namespace App\Interfaces;

interface ErrorInterface
{
    /**
     * Get error code
     *
     * @return int
     */
    public function code(): int;

    /**
     * Get error message
     *
     * @return string
     */
    public function message(): string;
}
