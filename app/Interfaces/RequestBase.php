<?php

namespace App\Interfaces;

use App\Http\Errors\ValidationError;
use App\Services\ErrorService;
use Illuminate\Contracts\Validation\Validator;
use Urameshibr\Requests\FormRequest;

abstract class RequestBase extends FormRequest
{
    /**
     * Always authorize a request
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Return error
     *
     * @param Validator $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        $errorService = app(ErrorService::class);
        $errorService->throw(new ValidationError, 422);
    }

    /**
     * Get only validated data
     *
     * @return array
     */
    public function validated(): array
    {
        $rules = $this->container->call([$this, 'rules']);
        return $this->only(collect($rules)->keys()->map(function ($rule) {
            return str_contains($rule, '.') ? explode('.', $rule)[0] : $rule;
        })->unique()->toArray());
    }
}
