<?php

namespace App\Interfaces;

interface ActionInterface
{
    /**
     * Perform checks before call action
     *
     * @return bool
     */
    public function before(): bool;

    /**
     * Treatment if check goes fail
     *
     * @return mixed
     */
    public function beforeError();

    /**
     * Runs action
     *
     * @return mixed
     */
    public function handle();

    /**
     * Perform the action
     *
     * @return mixed
     */
    public function action();
}
