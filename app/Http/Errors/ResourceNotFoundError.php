<?php

namespace App\Http\Errors;

use App\Interfaces\ErrorInterface;

class ResourceNotFoundError implements ErrorInterface
{
    /**
     * Get error code
     *
     * @return int
     */
    public function code(): int
    {
        return 1101;
    }

    /**
     * Get error message
     *
     * @return string
     */
    public function message(): string
    {
        return 'O recurso não foi encontrado.';
    }
}
