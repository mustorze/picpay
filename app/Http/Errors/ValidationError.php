<?php

namespace App\Http\Errors;

use App\Interfaces\ErrorInterface;

class ValidationError implements ErrorInterface
{
    /**
     * Get error code
     *
     * @return int
     */
    public function code(): int
    {
        return 1001;
    }

    /**
     * Get error message
     *
     * @return string
     */
    public function message(): string
    {
        return 'Ocorreu um erro de validação.';
    }
}
