<?php

namespace App\Http\Errors;

use App\Interfaces\ErrorInterface;

class UnauthorizedError implements ErrorInterface
{
    /**
     * Get error code
     *
     * @return int
     */
    public function code(): int
    {
        return 1201;
    }

    /**
     * Get error message
     *
     * @return string
     */
    public function message(): string
    {
        return 'O recurso não foi autorizado.';
    }
}
