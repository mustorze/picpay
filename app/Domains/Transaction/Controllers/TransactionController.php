<?php

namespace App\Domains\Transaction\Controllers;

use App\Domains\Transaction\Actions\TransactionShowAction;
use App\Domains\Transaction\Actions\TransactionTransferAction;
use App\Domains\User\Resources\TransactionResource;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    public function transfer(TransactionTransferAction $action)
    {
        return new TransactionResource($action->handle());
    }

    public function show($transactionId)
    {
        $action = app()->make(TransactionShowAction::class, ['transactionId' => $transactionId]);

        return new TransactionResource($action->handle());
    }
}
