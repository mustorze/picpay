<?php

namespace App\Domains\Transaction\Gateways;

use App\Domains\Transaction\Gateways\Responses\TransferResponse;
use App\Interfaces\GatewayInterface;

class PaymentGateway implements GatewayInterface
{
    /**
     * Make an account-to-account transaction
     *
     * @param int $payer
     * @param int $payee
     * @param $value
     * @return TransferResponse
     */
    public function transfer(int $payer, int $payee, $value): TransferResponse
    {
        // Third services checks
        $authorized = $this->haveBalance($payer, $value);

        // Check for payee existence Payment::exists($payee);
        // Make the transaction and returns data Payment::transact($payer, $payee, $value);
        return new TransferResponse($authorized, $authorized ? rand() : 0);
    }

    /**
     * Check for balance in account
     *
     * @param int $accountId
     * @param $value
     * @return bool
     */
    public function haveBalance(int $accountId, $value): bool
    {
        // Third services checks
        // Check for user existence Payment::exists($user);
        if ($value >= 100) {
            return false;
        }

        return true;
    }
}
