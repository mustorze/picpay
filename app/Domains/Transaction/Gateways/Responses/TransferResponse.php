<?php

namespace App\Domains\Transaction\Gateways\Responses;

class TransferResponse
{
    public $status;
    public $id;

    public function __construct(int $status, int $id)
    {
        $this->status = $status;
        $this->id = $id;
    }
}
