<?php

namespace App\Domains\Transaction\Actions;

use App\Domains\Transaction\Models\Transaction;
use App\Interfaces\ActionBase;

class TransactionShowAction extends ActionBase
{
    public $transactionId;

    public function __construct(string $transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * Find transaction by id
     *
     * @return Transaction|null
     */
    public function action(): ?Transaction
    {
        return Transaction::where('id', $this->transactionId)
            ->firstOrFail();
    }
}
