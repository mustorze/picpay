<?php

namespace App\Domains\Transaction\Actions;

use App\Domains\Transaction\Models\Transaction;
use App\Domains\Transaction\Requests\TransactionTransferRequest;
use App\Http\Errors\ResourceNotFoundError;
use App\Interfaces\ActionBase;
use App\Services\AccountService;
use App\Services\ErrorService;
use App\Services\TransactionService;
use Throwable;

class TransactionTransferAction extends ActionBase
{
    private $request;
    private $transactionService;
    private $accountService;

    public function __construct(TransactionTransferRequest $request, TransactionService $transactionService, AccountService $accountService)
    {
        $this->request = $request;
        $this->transactionService = $transactionService;
        $this->accountService = $accountService;
    }

    /**
     * Perform checks before call action
     *
     * @return bool
     */
    public function before(): bool
    {
        $data = $this->request->validated();

        $payer = $this->accountService
            ->exists($data['payer_id']);

        $payee = $this->accountService
            ->exists($data['payee_id']);

        return ($payer and $payee);
    }

    /**
     * Make an transaction
     *
     * @return Transaction
     * @throws Throwable
     */
    public function action(): Transaction
    {
        $data = $this->request->validated();

        return $this->transactionService
            ->transact(
                $data['payer_id'],
                $data['payee_id'],
                $data['value']
            );
    }

    /**
     * Treatment if check goes fail
     *
     * @return mixed
     */
    public function beforeError()
    {
        $errorService = app(ErrorService::class);
        return $errorService->throw(new ResourceNotFoundError, 404);
    }
}
