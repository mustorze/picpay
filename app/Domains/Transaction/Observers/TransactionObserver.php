<?php

namespace App\Domains\Transaction\Models;

use App\Domains\Transaction\Events\TransactionWasCreatedEvent;
use Carbon\Carbon;

class TransactionObserver
{
    public function creating(Transaction $transaction)
    {
        if (!isset($transaction->transaction_date)) {
            $transaction->transaction_date = Carbon::now()->toISOString();
        }
    }

    public function created(Transaction $transaction)
    {
        event(new TransactionWasCreatedEvent($transaction));
    }
}
