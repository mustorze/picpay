<?php

namespace App\Domains\Transaction\Requests;

use App\Interfaces\RequestBase;

class TransactionTransferRequest extends RequestBase
{
    public function rules()
    {
        return [
            'payer_id' => ['required', 'numeric'],
            'payee_id' => ['required', 'numeric'],
            'value' => ['required', 'numeric', 'min:0.01']
        ];
    }
}
