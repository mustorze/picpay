<?php

namespace App\Domains\Transaction\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'payee_id',
        'payer_id',
        'value',
        'transaction_date'
    ];

    protected $casts = [
        'payee_id' => 'int',
        'payer_id' => 'int',
        'value' => 'int'
    ];
}
