<?php

namespace App\Domains\Transaction\Events;

use App\Domains\Transaction\Models\Transaction;

class TransactionWasCreatedEvent
{
    public $transaction;

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }
}
