<?php

namespace App\Domains\User\Events\Listeners;

use App\Domains\Transaction\Events\TransactionWasCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogTransactionCreationListener implements ShouldQueue
{
    public function handle(TransactionWasCreatedEvent $event)
    {
        Log::info("A new transaction with payer ({$event->transaction->payer_id}) and payee ({$event->transaction->payee_id}) has been created");
    }
}
