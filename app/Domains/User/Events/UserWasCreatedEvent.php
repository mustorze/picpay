<?php

namespace App\Domains\User\Events;

use App\Domains\User\Models\User;

class UserWasCreatedEvent
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
