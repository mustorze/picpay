<?php

namespace App\Domains\User\Events;

use App\Domains\User\Models\Consumer;

class ConsumerWasCreatedEvent
{
    public $consumer;

    public function __construct(Consumer $consumer)
    {
        $this->consumer = $consumer;
    }
}
