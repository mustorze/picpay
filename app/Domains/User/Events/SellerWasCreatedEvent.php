<?php

namespace App\Domains\User\Events;

use App\Domains\User\Models\Seller;

class SellerWasCreatedEvent
{
    public $seller;

    public function __construct(Seller $seller)
    {
        $this->seller = $seller;
    }
}
