<?php

namespace App\Domains\User\Events\Listeners;

use App\Domains\User\Events\SellerWasCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogSellerCreationListener implements ShouldQueue
{
    public function handle(SellerWasCreatedEvent $event)
    {
        Log::info("A new seller with username ({$event->seller->username}) and ID ({$event->seller->id}) has been created");
    }
}
