<?php

namespace App\Domains\User\Events\Listeners;

use App\Domains\User\Events\ConsumerWasCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogConsumerCreationListener implements ShouldQueue
{
    public function handle(ConsumerWasCreatedEvent $event)
    {
        Log::info("A new consumer with username ({$event->consumer->username}) and ID ({$event->consumer->id}) has been created");
    }
}
