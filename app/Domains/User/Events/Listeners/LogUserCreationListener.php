<?php

namespace App\Domains\User\Events\Listeners;

use App\Domains\User\Events\UserWasCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogUserCreationListener implements ShouldQueue
{
    public function handle(UserWasCreatedEvent $event)
    {
        Log::info("A new user with name ({$event->user->full_name}) and ID ({$event->user->id}) has been created");
    }
}
