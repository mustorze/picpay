<?php

namespace App\Domains\User\Actions;

use App\Domains\User\Models\User;
use App\Domains\User\Requests\UserIndexRequest;
use App\Interfaces\ActionBase;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

class IndexUserAction extends ActionBase
{
    public $request;

    public function __construct(UserIndexRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Get users and filter by accounts
     *
     * @return Collection|null
     */
    public function action(): ?Collection
    {
        $param = $this->request->get('q');

        return Cache::tags(['users'])->remember($param ?? 'index', Carbon::now()->addHour(), function () use ($param) {
            return User::query()->when($param, function ($query, $param) {
                $query->findWithAccounts($param);
            })
                ->get();
        });
    }
}
