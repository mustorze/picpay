<?php

namespace App\Domains\User\Actions;

use App\Domains\User\Models\Seller;
use App\Domains\User\Models\User;
use App\Domains\User\Requests\SellerStoreRequest;
use App\Http\Errors\ResourceNotFoundError;
use App\Interfaces\ActionBase;
use App\Services\ErrorService;

class CreateSellerAction extends ActionBase
{
    public $request;

    public function __construct(SellerStoreRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Perform checks before call action
     *
     * @return bool
     */
    public function before(): bool
    {
        $userId = $this->request->post('user_id');

        return User::where('id', $userId)->exists();
    }

    /**
     * Creates an seller
     *
     * @return Seller
     */
    public function action(): Seller
    {
        return Seller::create($this->request->validated());
    }

    /**
     * Treatment if check goes fail
     *
     * @return mixed
     */
    public function beforeError()
    {
        $errorService = app(ErrorService::class);
        return $errorService->throw(new ResourceNotFoundError, 404);
    }
}
