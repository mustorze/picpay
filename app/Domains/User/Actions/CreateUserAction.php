<?php

namespace App\Domains\User\Actions;

use App\Domains\User\Models\User;
use App\Domains\User\Requests\UserStoreRequest;
use App\Interfaces\ActionBase;

class CreateUserAction extends ActionBase
{
    public $request;

    public function __construct(UserStoreRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Creates an user
     *
     * @return User
     */
    public function action(): User
    {
        return User::create($this->request->validated());
    }
}
