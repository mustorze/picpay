<?php

namespace App\Domains\User\Actions;

use App\Domains\User\Models\User;

use App\Interfaces\ActionBase;

class ShowUserAction extends ActionBase
{
    public $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * Get an user with accounts
     *
     * @return User|null
     */
    public function action(): ?User
    {
        return User::where('id', $this->userId)
            ->with(['consumer', 'seller'])
            ->firstOrFail();
    }
}
