<?php

namespace App\Domains\User\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $fillable = [
        'user_id',
        'username',
        'cnpj',
        'fantasy_name',
        'social_name'
    ];
}
