<?php

namespace App\Domains\User\Models;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{
    protected $fillable = [
        'user_id',
        'username'
    ];
}
