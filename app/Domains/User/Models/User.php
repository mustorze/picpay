<?php

namespace App\Domains\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    protected $fillable = [
        'name',
        'cpf',
        'email',
        'full_name',
        'password',
        'phone_number'
    ];

    /**
     * Get his consumer account
     *
     * @return HasOne
     */
    public function consumer()
    {
        return $this->hasOne(Consumer::class);
    }

    /**
     * Get his seller account
     *
     * @return HasOne
     */
    public function seller()
    {
        return $this->hasOne(Seller::class);
    }

    /**
     * Query scope for find an name between his accounts
     *
     * @param Builder $query
     * @param $param
     * @return Builder
     */
    public function scopeFindWithAccounts(Builder $query, $param)
    {
        return $query->where('full_name', 'LIKE', "$param%")
            ->orWhere(function ($query) use ($param) {
                $query->whereExists(function ($query) use ($param) {
                    $query->select('id')
                        ->from('consumers')
                        ->where('consumers.user_id', DB::raw('users.id'))
                        ->where('username', 'LIKE', "$param%");
                })
                    ->orWhereExists(function ($query) use ($param) {
                        $query->select('id')
                            ->from('sellers')
                            ->where('sellers.user_id', DB::raw('users.id'))
                            ->where('username', 'LIKE', "$param%");
                    });
            });
    }
}
