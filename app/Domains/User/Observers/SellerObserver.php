<?php

namespace App\Domains\User\Requests;

use App\Domains\User\Events\SellerWasCreatedEvent;
use App\Domains\User\Models\Seller;

class SellerObserver
{
    public function created(Seller $seller)
    {
        event(new SellerWasCreatedEvent($seller));
    }
}
