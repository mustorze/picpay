<?php

namespace App\Domains\User\Requests;

use App\Domains\User\Events\UserWasCreatedEvent;
use App\Domains\User\Models\User;
use Illuminate\Support\Facades\Cache;

class UserObserver
{
    public function created(User $user)
    {
        event(new UserWasCreatedEvent($user));
    }

    public function saved(User $user)
    {
        Cache::tags(['users'])->flush();
    }
}
