<?php

namespace App\Domains\User\Requests;

use App\Domains\User\Events\ConsumerWasCreatedEvent;
use App\Domains\User\Models\Consumer;

class ConsumerObserver
{
    public function created(Consumer $consumer)
    {
        event(new ConsumerWasCreatedEvent($consumer));
    }
}
