<?php

namespace App\Domains\User\Controllers;

use App\Domains\User\Actions\CreateSellerAction;
use App\Domains\User\Resources\SellerResource;
use App\Http\Controllers\Controller;

class SellerController extends Controller
{
    public function store(CreateSellerAction $action)
    {
        return new SellerResource($action->handle());
    }
}
