<?php

namespace App\Domains\User\Controllers;

use App\Domains\User\Actions\CreateUserAction;
use App\Domains\User\Actions\IndexUserAction;
use App\Domains\User\Actions\ShowUserAction;
use App\Domains\User\Resources\UserResource;
use App\Domains\User\Resources\UserShowResource;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function store(CreateUserAction $action)
    {
        return new UserResource($action->action());
    }

    public function index(IndexUserAction $action)
    {
        return UserResource::collection($action->action());
    }

    public function show($userId)
    {
        $action = app()->make(ShowUserAction::class, ['userId' => $userId]);

        return new UserShowResource($action->action());
    }
}
