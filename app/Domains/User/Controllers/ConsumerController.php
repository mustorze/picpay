<?php

namespace App\Domains\User\Controllers;

use App\Domains\User\Actions\CreateConsumerAction;
use App\Domains\User\Resources\ConsumerResource;
use App\Http\Controllers\Controller;

class ConsumerController extends Controller
{
    public function store(CreateConsumerAction $action)
    {
        return new ConsumerResource($action->handle());
    }
}
