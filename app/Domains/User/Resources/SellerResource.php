<?php

namespace App\Domains\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SellerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'username' => $this->username,
            'cnpj' => $this->cnpj,
            'fantasy_name' => $this->fantasy_name,
            'social_name' => $this->social_name
        ];
    }

    public function withResponse($request, $response)
    {
        if ($request->method() == 'POST') {
            $response->setStatusCode(201);
            return;
        }

        $response->setStatusCode(200);
    }
}
