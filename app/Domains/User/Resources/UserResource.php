<?php

namespace App\Domains\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'cpf' => $this->cpf,
            'email' => $this->email,
            'full_name' => $this->full_name,
            'id' => $this->id,
            'password' => $this->password,
            'phone_number' => $this->phone_number
        ];
    }

    public function withResponse($request, $response)
    {
        if ($request->method() == 'POST') {
            $response->setStatusCode(201);
            return;
        }

        $response->setStatusCode(200);
    }
}
