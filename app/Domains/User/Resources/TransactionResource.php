<?php

namespace App\Domains\User\Resources;

use App\Services\TransactionService;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    public function toArray($request)
    {
        $service = app(TransactionService::class);

        return [
            'id' => $this->id,
            'payee_id' => $this->payee_id,
            'payer_id' => $this->payer_id,
            'transaction_date' => $this->transaction_date,
            'value' => $service->toFloat($this->value)
        ];
    }

    public function withResponse($request, $response)
    {
        if ($request->method() == 'POST') {
            $response->setStatusCode(201);
            return;
        }

        $response->setStatusCode(200);
    }
}
