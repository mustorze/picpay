<?php

namespace App\Domains\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ConsumerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'username' => $this->username
        ];
    }

    public function withResponse($request, $response)
    {
        if ($request->method() == 'POST') {
            $response->setStatusCode(201);
            return;
        }

        $response->setStatusCode(200);
    }
}
