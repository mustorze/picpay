<?php

namespace App\Domains\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserShowResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'accounts' => [
                'consumer' => new ConsumerResource($this->consumer),
                'seller' => new SellerResource($this->seller)
            ],
            'user' => new UserResource($this)
        ];
    }

    public function withResponse($request, $response)
    {
        if ($request->method() == 'POST') {
            $response->setStatusCode(201);
            return;
        }

        $response->setStatusCode(200);
    }
}
