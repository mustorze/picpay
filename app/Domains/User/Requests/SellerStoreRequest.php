<?php

namespace App\Domains\User\Requests;

use App\Interfaces\RequestBase;

class SellerStoreRequest extends RequestBase
{
    public function rules()
    {
        return [
            'user_id' => ['required', 'unique:sellers'],
            'username' => ['required', 'unique:consumers', 'unique:sellers'],
            'cnpj' => ['required'],
            'fantasy_name' => ['required'],
            'social_name' => ['required']
        ];
    }
}
