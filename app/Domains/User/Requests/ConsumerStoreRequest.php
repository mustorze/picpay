<?php

namespace App\Domains\User\Requests;

use App\Interfaces\RequestBase;

class ConsumerStoreRequest extends RequestBase
{
    public function rules()
    {
        return [
            'user_id' => ['required', 'unique:consumers'],
            'username' => ['required', 'unique:consumers', 'unique:sellers'],
        ];
    }
}
