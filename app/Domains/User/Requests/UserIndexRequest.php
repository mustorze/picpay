<?php

namespace App\Domains\User\Requests;

use App\Interfaces\RequestBase;

class UserIndexRequest extends RequestBase
{
    public function rules()
    {
        return [
            'q' => ['sometimes'],
        ];
    }
}
