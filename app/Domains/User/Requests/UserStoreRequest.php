<?php

namespace App\Domains\User\Requests;

use App\Http\Rules\CpfRule;
use App\Interfaces\RequestBase;

class UserStoreRequest extends RequestBase
{
    public function rules()
    {
        return [
            'cpf' => ['required', new CpfRule, 'unique:users,cpf'],
            'email' => ['required', 'email', 'unique:users,email'],
            'full_name' => ['required', 'string'],
            'password' => ['required', 'string', 'min:6'],
            'phone_number' => ['required', 'string']
        ];
    }
}
