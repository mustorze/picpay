<?php

namespace App\Exceptions;

use App\Interfaces\ErrorInterface;
use Exception;

class GenericException extends Exception
{
    public $error;

    public function __construct(ErrorInterface $error, $code)
    {
        $this->error = $error;
        parent::__construct($this->getMessage(), $code);
    }

    public function render($request)
    {
        return response()->json([
            'code' => $this->error->code(),
            'message' => $this->error->message()
        ], $this->getCode());
    }
}
