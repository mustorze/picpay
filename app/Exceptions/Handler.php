<?php

namespace App\Exceptions;

use App\Http\Errors\ResourceNotFoundError;
use App\Services\ErrorService;
use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        GenericException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        $errorService = app(ErrorService::class);

        if ($exception instanceof GenericException) {
            return response()->json([
                'code' => $exception->error->code(),
                'message' => $exception->error->message()
            ], $exception->getCode() ?? 500);
        }

        if ($exception instanceof ModelNotFoundException) {
            return $errorService->response(new ResourceNotFoundError, 404);
        }

        return parent::render($request, $exception);
    }
}
