# Serviço de Cadastro de Usuários e Transações
API Utilizada para gestão do Cadastro de Usuários e Transações do PicPay

* [Notas Gerais](docs/general-notes.md)
* [API Swagger](docs/api-docs.json)

## Começando
Siga os passos abaixo para utilizar a API

## Ambiente de desenvolvimento
### Pré-requisitos
* [Docker](https://docs.docker.com/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)

### Iniciando o ambiente
Abra a pasta do projeto e execute:
```bash
docker-compose up -d
```

### Instalando
Copie e configure suas variáveis de ambiente
```bash
cp .env.example .env
```

Instale as dependências com o composer
```bash
docker exec -it users-api-php composer install
```

E está pronto seu ambiente de desenvolvimento

### Executando os testes do projeto
A suíte de teste foi feita com o PHPUnit e pode ser executada utilizando o comando abaixo
```bash
docker exec -it users-api-php php ./vendor/bin/phpunit --coverage-text
```

Saída:
```bash
PHPUnit 7.5.16 by Sebastian Bergmann and contributors.
...................................................       51 / 51 (100%)
Time: 28.51 seconds, Memory: 14.00 MB

OK (51 tests, 187 assertions)

Code Coverage Report:
  2019-09-25 15:14:21

 Summary:
  Classes: 79.59% (39/49)
  Methods: 88.89% (80/90)
  Lines:   92.18% (224/243)
```

## Guia de estilo
Para checar a guia de estilo, execute:
```bash
docker exec -it users-api-php php-cs-fixer fix . -v
```

Saída:
```bash
Loaded config default.
..................................................................................
Legend: ?-unknown, I-invalid file syntax, file ignored, S-Skipped, .-no changes, F-fixed, E-error
Fixed all files in 0.851 seconds, 10.000 MB memory used
```

## Feito com
* [Lumen](https://lumen.laravel.com) - The stunningly fast micro-framework by Laravel
* [Composer](https://getcomposer.org) - Dependency Manager for PHP

## Autores
* **Henrique Duarte** - [GitHub](https://github.com/mustorze) - [GitLab](https://gitlab.com/mustorze)
