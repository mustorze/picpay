<?php

use App\Domains\User\Models\User;
use App\Http\Errors\ResourceNotFoundError;
use Laravel\Lumen\Testing\DatabaseMigrations;

class UserShowTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_api_can_get_user_by_id()
    {
        $user = factory(User::class)->create();

        $this->json('GET', '/users/' . $user->id)
            ->assertResponseStatus(200);
    }

    /** @test */
    public function a_api_cannot_find_user_with_id()
    {
        $this->json('GET', '/users/1')
            ->assertResponseStatus(404);

        $this->assertExpectedError(new ResourceNotFoundError);
    }
}
