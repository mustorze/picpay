<?php

use App\Domains\User\Models\Seller;
use App\Domains\User\Models\User;
use App\Http\Errors\ResourceNotFoundError;
use App\Http\Errors\ValidationError;
use Laravel\Lumen\Testing\DatabaseMigrations;

class SellerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function api_can_create_a_new_seller()
    {
        $sellerData = factory(Seller::class)->make()
            ->toArray();

        $this->json('POST', '/users/sellers', $sellerData);

        $response = json_decode($this->response->getContent());

        $expected = array_merge($sellerData, [
            'id' => $response->id ?? 0
        ]);

        $this->assertJsonStringEqualsJsonString(json_encode($expected), $this->response->getContent());
    }

    /** @test */
    public function api_cannot_create_seller_with_invalid_user_id()
    {
        $sellerData = factory(Seller::class)->make(['user_id' => 1])
            ->toArray();

        $this->json('POST', '/users/sellers', $sellerData)
            ->assertResponseStatus(404);

        $this->assertExpectedError(new ResourceNotFoundError);
    }

    /** @test */
    public function api_cannot_create_seller_with_invalid_username()
    {
        $sellerData = factory(Seller::class)->make(['username' => null])
            ->toArray();

        $this->json('POST', '/users/sellers', $sellerData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_create_seller_with_non_unique_username()
    {
        $username = $this->faker->slug;
        factory(Seller::class)->create(['username' => $username]);

        $sellerData = factory(Seller::class)->make(['username' => $username])
            ->toArray();

        $this->json('POST', '/users/sellers', $sellerData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_create_seller_when_user_already_have_one()
    {
        $user = factory(User::class)->create();
        factory(Seller::class)->create(['user_id' => $user->id]);

        $sellerData = factory(Seller::class)->make(['user_id' => $user->id])
            ->toArray();

        $this->json('POST', '/users/sellers', $sellerData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }
}
