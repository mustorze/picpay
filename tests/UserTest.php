<?php

use App\Domains\User\Events\UserWasCreatedEvent;
use App\Domains\User\Models\User;
use App\Http\Errors\ValidationError;
use Illuminate\Support\Facades\Event;
use Laravel\Lumen\Testing\DatabaseMigrations;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function api_create_a_new_user()
    {
        $userData = factory(User::class)->make()
            ->toArray();

        $this->json('POST', '/users', $userData);

        $response = json_decode($this->response->getContent());

        $expected = array_merge($userData, [
            'id' => $response->id ?? 0
        ]);

        $this->assertJsonStringEqualsJsonString(json_encode($expected), $this->response->getContent());
    }

    /** @test */
    public function api_cannot_create_user_with_invalid_cpf()
    {
        $userData = factory(User::class)->make(['cpf' => '11111111111'])
            ->toArray();

        $this->json('POST', '/users', $userData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_create_user_with_non_unique_cpf()
    {
        $cpf = $this->faker->cpf;

        factory(User::class)->create(['cpf' => $cpf]);
        $userData = factory(User::class)->make(['cpf' => $cpf])
            ->toArray();

        $this->json('POST', '/users', $userData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_create_user_with_invalid_email()
    {
        $userData = factory(User::class)->make(['email' => 'anything.com'])
            ->toArray();

        $this->json('POST', '/users', $userData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_create_user_with_non_unique_email()
    {
        $email = $this->faker->email;

        factory(User::class)->create(['email' => $email]);
        $userData = factory(User::class)->make(['email' => $email])
            ->toArray();

        $this->json('POST', '/users', $userData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_create_user_without_name()
    {
        $userData = factory(User::class)->make(['full_name' => null])
            ->toArray();

        $this->json('POST', '/users', $userData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_create_user_without_phone()
    {
        $userData = factory(User::class)->make(['phone_number' => null])
            ->toArray();

        $this->json('POST', '/users', $userData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_create_user_with_invalid_password()
    {
        $userData = factory(User::class)->make(['password' => 'pass'])
            ->toArray();

        $this->json('POST', '/users', $userData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }
}
