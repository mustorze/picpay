<?php

use App\Domains\Transaction\Gateways\Responses\TransferResponse;
use App\Domains\User\Models\Consumer;
use App\Domains\User\Models\Seller;
use App\Exceptions\GenericException;
use App\Interfaces\GatewayInterface;
use App\Services\TransactionService;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Mockery\LegacyMockInterface;

class TransactionUnitTest extends TestCase
{
    use DatabaseMigrations;

    public $faker;
    public $service;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Factory::create('pt_BR');
        $this->service = app(TransactionService::class);

        $this->app->instance(GatewayInterface::class, Mockery::mock(GatewayInterface::class, function (LegacyMockInterface $mock) {
            $mock->shouldReceive('transfer')
                ->withAnyArgs()
                ->andReturnUsing(function (...$args) {
                    $value = $args[2];

                    $balance = function ($value) {
                        if ($value >= 100) {
                            return false;
                        }

                        return true;
                    };

                    $authorized = $balance($value);
                    return new TransferResponse($authorized, $authorized ? rand() : 0);
                });
        }));
    }

    /** @test */
    public function it_can_make_a_transfer_between_seller_and_consumer_bellow_100()
    {
        $seller = factory(Seller::class)->create();
        $consumer = factory(Consumer::class)->create();
        $value = rand(1, 99);

        $transaction = $this->service->transact($seller->id, $consumer->id, $value);

        $expectedTransaction = [
            'payer_id' => $seller->id,
            'payee_id' => $consumer->id,
            'value' => $value
        ];

        $this->assertJson(json_encode($expectedTransaction), json_encode($transaction->toArray()));
    }

    /** @test */
    public function it_can_make_a_transfer_between_consumer_and_seller_bellow_100()
    {
        $seller = factory(Seller::class)->create();
        $consumer = factory(Consumer::class)->create();
        $value = rand(1, 99);

        $transaction = $this->service->transact($consumer->id, $seller->id, $value);

        $expectedTransaction = [
            'payer_id' => $consumer->id,
            'payee_id' => $seller->id,
            'value' => $value
        ];

        $this->assertJson(json_encode($expectedTransaction), json_encode($transaction->toArray()));
    }

    /** @test */
    public function it_can_make_a_transfer_between_consumers_bellow_100()
    {
        $consumer_1 = factory(Consumer::class)->create();
        $consumer_2 = factory(Consumer::class)->create();
        $value = rand(1, 99);

        $transaction = $this->service->transact($consumer_1->id, $consumer_2->id, $value);

        $expectedTransaction = [
            'payer_id' => $consumer_1->id,
            'payee_id' => $consumer_2->id,
            'value' => $value
        ];

        $this->assertJson(json_encode($expectedTransaction), json_encode($transaction->toArray()));
    }

    /** @test */
    public function it_can_make_a_transfer_between_sellers_bellow_100()
    {
        $seller_1 = factory(Seller::class)->create();
        $seller_2 = factory(Seller::class)->create();
        $value = rand(1, 99);

        $transaction = $this->service->transact($seller_1->id, $seller_2->id, $value);

        $expectedTransaction = [
            'payer_id' => $seller_1->id,
            'payee_id' => $seller_2->id,
            'value' => $value
        ];

        $this->assertJson(json_encode($expectedTransaction), json_encode($transaction->toArray()));
    }

    /** @test */
    public function it_cannot_make_a_transfer_between_seller_and_consumer_above_100()
    {
        $seller = factory(Seller::class)->create();
        $consumer = factory(Consumer::class)->create();
        $value = rand(100, 9999999);

        $this->expectException(GenericException::class);
        $this->service->transact($seller->id, $consumer->id, $value);
    }

    /** @test */
    public function it_cannot_make_a_transfer_between_consumer_and_seller_above_100()
    {
        $seller = factory(Seller::class)->create();
        $consumer = factory(Consumer::class)->create();
        $value = rand(100, 9999999);

        $this->expectException(GenericException::class);
        $this->service->transact($consumer->id, $seller->id, $value);
    }

    /** @test */
    public function it_cannot_make_a_transfer_between_consumers_above_100()
    {
        $consumer_1 = factory(Consumer::class)->create();
        $consumer_2 = factory(Consumer::class)->create();
        $value = rand(100, 9999999);

        $this->expectException(GenericException::class);
        $this->service->transact($consumer_1->id, $consumer_2->id, $value);
    }

    /** @test */
    public function it_cannot_make_a_transfer_between_sellers_above_100()
    {
        $seller_1 = factory(Seller::class)->create();
        $seller_2 = factory(Seller::class)->create();
        $value = rand(100, 9999999);

        $this->expectException(GenericException::class);
        $this->service->transact($seller_1->id, $seller_2->id, $value);
    }
}
