<?php

use App\Domains\User\Models\Consumer;
use App\Domains\User\Models\Seller;
use App\Services\AccountService;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class AccountUnitTest extends TestCase
{
    use DatabaseMigrations;

    public $faker;
    public $service;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Factory::create('pt_BR');
        $this->service = app(AccountService::class);
    }

    /** @test */
    public function it_get_success_when_check_for_existent_consumer_account()
    {
        $consumer = factory(Consumer::class)->create();

        $this->assertTrue($this->service->exists($consumer->id));
    }

    /** @test */
    public function it_get_success_when_check_for_existent_seller_account()
    {
        $seller = factory(Seller::class)->create();

        $this->assertTrue($this->service->exists($seller->id));
    }

    /** @test */
    public function it_get_error_when_check_for_nonexistent_account()
    {
        $this->assertFalse($this->service->exists(1));
    }
}
