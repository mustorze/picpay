<?php

use App\Domains\User\Models\Consumer;
use App\Domains\User\Models\User;
use App\Http\Errors\ResourceNotFoundError;
use App\Http\Errors\ValidationError;
use Laravel\Lumen\Testing\DatabaseMigrations;

class ConsumerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function api_can_create_a_new_consumer()
    {
        $consumerData = factory(Consumer::class)->make()
            ->toArray();

        $this->json('POST', '/users/consumers', $consumerData);

        $response = json_decode($this->response->getContent());

        $expected = array_merge($consumerData, [
            'id' => $response->id ?? 0
        ]);

        $this->assertJsonStringEqualsJsonString(json_encode($expected), $this->response->getContent());
    }

    /** @test */
    public function api_cannot_create_consumer_with_invalid_user_id()
    {
        $consumerData = factory(Consumer::class)->make(['user_id' => 1])
            ->toArray();

        $this->json('POST', '/users/consumers', $consumerData)
            ->assertResponseStatus(404);

        $this->assertExpectedError(new ResourceNotFoundError);
    }

    /** @test */
    public function api_cannot_create_consumer_with_invalid_username()
    {
        $consumerData = factory(Consumer::class)->make(['username' => null])
            ->toArray();

        $this->json('POST', '/users/consumers', $consumerData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_create_consumer_with_non_unique_username()
    {
        $username = $this->faker->slug;
        factory(Consumer::class)->create(['username' => $username]);

        $consumerData = factory(Consumer::class)->make(['username' => $username])
            ->toArray();

        $this->json('POST', '/users/consumers', $consumerData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_create_consumer_when_user_already_have_one()
    {
        $user = factory(User::class)->create();
        factory(Consumer::class)->create(['user_id' => $user->id]);

        $consumerData = factory(Consumer::class)->make(['user_id' => $user->id])
            ->toArray();

        $this->json('POST', '/users/consumers', $consumerData)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }
}
