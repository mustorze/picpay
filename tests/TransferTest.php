<?php

use App\Domains\Transaction\Gateways\Responses\TransferResponse;
use App\Domains\Transaction\Models\Transaction;
use App\Domains\User\Models\Consumer;
use App\Domains\User\Models\Seller;
use App\Domains\User\Resources\TransactionResource;
use App\Http\Errors\ResourceNotFoundError;
use App\Http\Errors\UnauthorizedError;
use App\Http\Errors\ValidationError;
use App\Interfaces\GatewayInterface;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Mockery\LegacyMockInterface;

class TransferTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->app->instance(GatewayInterface::class, Mockery::mock(GatewayInterface::class, function (LegacyMockInterface $mock) {
            $mock->shouldReceive('transfer')
                ->withAnyArgs()
                ->andReturnUsing(function (...$args) {
                    $value = $args[2];

                    $balance = function ($value) {
                        if ($value >= 100) {
                            return false;
                        }

                        return true;
                    };

                    $authorized = $balance($value);
                    return new TransferResponse($authorized, $authorized ? rand() : 0);
                });
        }));
    }

    /** @test */
    public function api_can_make_a_transfer_between_seller_and_consumer_bellow_100()
    {
        $seller = factory(Seller::class)->create();
        $consumer = factory(Consumer::class)->create();

        $data = [
            'payer_id' => $seller->id,
            'payee_id' => $consumer->id,
            'value' => rand(1, 99)
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(200);

        $expectedTransaction = (new TransactionResource(Transaction::first()))->toArray($this->response);

        $this->assertJsonStringEqualsJsonString(json_encode($expectedTransaction), $this->response->getContent());
    }

    /** @test */
    public function api_can_make_a_transfer_between_consumer_and_seller_bellow_100()
    {
        $seller = factory(Seller::class)->create();
        $consumer = factory(Consumer::class)->create();

        $data = [
            'payer_id' => $consumer->id,
            'payee_id' => $seller->id,
            'value' => rand(1, 99)
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(200);

        $expectedTransaction = (new TransactionResource(Transaction::first()))->toArray($this->response);

        $this->assertJsonStringEqualsJsonString(json_encode($expectedTransaction), $this->response->getContent());
    }

    /** @test */
    public function api_can_make_a_transfer_between_consumers_bellow_100()
    {
        $consumer_1 = factory(Consumer::class)->create();
        $consumer_2 = factory(Consumer::class)->create();

        $data = [
            'payer_id' => $consumer_1->id,
            'payee_id' => $consumer_2->id,
            'value' => rand(1, 99)
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(200);

        $expectedTransaction = (new TransactionResource(Transaction::first()))->toArray($this->response);

        $this->assertJsonStringEqualsJsonString(json_encode($expectedTransaction), $this->response->getContent());
    }

    /** @test */
    public function api_can_make_a_transfer_between_sellers_bellow_100()
    {
        $seller_1 = factory(Seller::class)->create();
        $seller_2 = factory(Seller::class)->create();

        $data = [
            'payer_id' => $seller_1->id,
            'payee_id' => $seller_2->id,
            'value' => rand(1, 99)
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(200);

        $expectedTransaction = (new TransactionResource(Transaction::first()))->toArray($this->response);

        $this->assertJsonStringEqualsJsonString(json_encode($expectedTransaction), $this->response->getContent());
    }

    /** @test */
    public function api_cannot_make_a_transfer_between_seller_and_consumer_above_100()
    {
        $seller = factory(Seller::class)->create();
        $consumer = factory(Consumer::class)->create();

        $data = [
            'payer_id' => $seller->id,
            'payee_id' => $consumer->id,
            'value' => rand(100, 9999999)
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(401);

        $this->assertExpectedError(new UnauthorizedError);
    }

    /** @test */
    public function api_cannot_make_a_transfer_between_consumer_and_seller_above_100()
    {
        $seller = factory(Seller::class)->create();
        $consumer = factory(Consumer::class)->create();

        $data = [
            'payer_id' => $consumer->id,
            'payee_id' => $seller->id,
            'value' => rand(100, 9999999)
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(401);

        $this->assertExpectedError(new UnauthorizedError);
    }

    /** @test */
    public function api_cannot_make_a_transfer_between_consumers_above_100()
    {
        $consumer_1 = factory(Consumer::class)->create();
        $consumer_2 = factory(Consumer::class)->create();

        $data = [
            'payer_id' => $consumer_1->id,
            'payee_id' => $consumer_2->id,
            'value' => rand(100, 9999999)
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(401);

        $this->assertExpectedError(new UnauthorizedError);
    }

    /** @test */
    public function api_cannot_make_a_transfer_between_sellers_above_100()
    {
        $seller_1 = factory(Seller::class)->create();
        $seller_2 = factory(Seller::class)->create();

        $data = [
            'payer_id' => $seller_1->id,
            'payee_id' => $seller_2->id,
            'value' => rand(100, 9999999)
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(401);

        $this->assertExpectedError(new UnauthorizedError);
    }

    /** @test */
    public function api_cannot_make_a_transfer_from_seller_to_unknown_payee()
    {
        $seller = factory(Seller::class)->create();

        $data = [
            'payer_id' => $seller->id,
            'payee_id' => 2,
            'value' => rand(1, 99)
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(404);

        $this->assertExpectedError(new ResourceNotFoundError);
    }

    /** @test */
    public function api_cannot_make_a_transfer_from_consumer_to_unknown_payee()
    {
        $consumer = factory(Consumer::class)->create();

        $data = [
            'payer_id' => $consumer->id,
            'payee_id' => 2,
            'value' => rand(1, 99)
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(404);

        $this->assertExpectedError(new ResourceNotFoundError);
    }

    /** @test */
    public function api_cannot_make_a_transfer_from_consumer_if_value_bellow_0()
    {
        $consumer = factory(Consumer::class)->create();
        $seller = factory(Seller::class)->create();

        $data = [
            'payer_id' => $consumer->id,
            'payee_id' => $seller->id,
            'value' => -1
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_cannot_make_a_transfer_from_seller_if_value_bellow_0()
    {
        $seller = factory(Seller::class)->create();
        $consumer = factory(Consumer::class)->create();

        $data = [
            'payer_id' => $seller->id,
            'payee_id' => $consumer->id,
            'value' => -1
        ];

        $this->json('POST', '/transactions', $data)
            ->assertResponseStatus(422);

        $this->assertExpectedError(new ValidationError);
    }

    /** @test */
    public function api_can_get_a_existent_transaction_from_id()
    {
        $transaction = factory(Transaction::class)->create();

        $this->json('GET', '/transactions/' . $transaction->id)
            ->assertResponseStatus(200);

        $expectedTransaction = (new TransactionResource($transaction))->toArray($this->response);

        $this->assertJsonStringEqualsJsonString(json_encode($expectedTransaction), $this->response->getContent());
    }

    /** @test */
    public function api_can_get_a_nonexistent_transaction_from_id()
    {
        $this->json('GET', '/transactions/1')
            ->assertResponseStatus(404);

        $this->assertExpectedError(new ResourceNotFoundError);
    }
}
