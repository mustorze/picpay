<?php

use App\Domains\User\Models\Consumer;
use App\Domains\User\Models\Seller;
use App\Domains\User\Models\User;
use App\Domains\User\Resources\UserResource;
use Laravel\Lumen\Testing\DatabaseMigrations;

class UserIndexTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_api_can_search_all_users()
    {
        $users = factory(User::class, 2)->create();

        $this->json('GET', '/users')
            ->assertResponseStatus(200);

        $usersData = [];
        foreach ($users as $user) {
            $usersData[] = (new UserResource($user))->toArray($this->response);
        }

        $this->assertJsonStringEqualsJsonString(json_encode($usersData), $this->response->getContent());
    }

    /** @test */
    public function a_api_can_search_users_filtering_by_name()
    {
        $user = factory(User::class)->create(['full_name' => 'filter']);

        $this->json('GET', '/users?q=fil')
            ->assertResponseStatus(200);

        $usersData[] = (new UserResource($user))->toArray($this->response);

        $this->assertJsonStringEqualsJsonString(json_encode($usersData), $this->response->getContent());
    }

    /** @test */
    public function a_api_can_search_users_filtering_by_consumer_account_username()
    {
        $user = factory(User::class)->create(['full_name' => 'any']);
        factory(Consumer::class)->create(['username' => 'filter', 'user_id' => $user->id]);

        $this->json('GET', '/users?q=fil')
            ->assertResponseStatus(200);

        $usersData[] = (new UserResource($user))->toArray($this->response);

        $this->assertJsonStringEqualsJsonString(json_encode($usersData), $this->response->getContent());
    }

    /** @test */
    public function a_api_can_search_users_filtering_by_seller_account_username()
    {
        $user = factory(User::class)->create(['full_name' => 'any']);
        factory(Seller::class)->create(['username' => 'filter', 'user_id' => $user->id]);

        $this->json('GET', '/users?q=fil')
            ->assertResponseStatus(200);

        $usersData[] = (new UserResource($user))->toArray($this->response);

        $this->assertJsonStringEqualsJsonString(json_encode($usersData), $this->response->getContent());
    }

    /** @test */
    public function a_api_cannot_find_any_user()
    {
        $this->json('GET', '/users')
            ->assertResponseStatus(200);

        $this->assertJsonStringEqualsJsonString(json_encode([]), $this->response->getContent());
    }

    /** @test */
    public function a_api_cannot_find_any_user_filtering()
    {
        $user = factory(User::class)->create(['full_name' => 'any']);
        factory(Consumer::class)->create(['username' => 'foo', 'user_id' => $user->id]);

        $this->json('GET', '/users?q=fil')
            ->assertResponseStatus(200);

        $this->assertJsonStringEqualsJsonString(json_encode([]), $this->response->getContent());
    }
}
