<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['as' => 'transactions', 'prefix' => 'transactions', 'name' => 'transactions'], function ($router) {
    $router->post('/', [
        'uses' => 'App\Domains\Transaction\Controllers\TransactionController@transfer',
        'as' => 'transfer'
    ]);

    $router->get('/{transactionId}', [
        'uses' => 'App\Domains\Transaction\Controllers\TransactionController@show',
        'as' => 'show'
    ]);
});

$router->group(['as' => 'users', 'prefix' => 'users', 'name' => 'users'], function ($router) {
    $router->post('/', [
        'uses' => 'App\Domains\User\Controllers\UserController@store',
        'as' => 'store'
    ]);

    $router->get('/', [
        'uses' => 'App\Domains\User\Controllers\UserController@index',
        'as' => 'index'
    ]);

    $router->get('/{userId}', [
        'uses' => 'App\Domains\User\Controllers\UserController@show',
        'as' => 'show'
    ]);

    $router->group(['as' => 'consumers', 'prefix' => 'consumers', 'name' => 'consumers'], function ($router) {
        $router->post('/', [
            'uses' => 'App\Domains\User\Controllers\ConsumerController@store',
            'as' => 'store'
        ]);
    });

    $router->group(['as' => 'sellers', 'prefix' => 'sellers', 'name' => 'sellers'], function ($router) {
        $router->post('/', [
            'uses' => 'App\Domains\User\Controllers\SellerController@store',
            'as' => 'store'
        ]);
    });
});
