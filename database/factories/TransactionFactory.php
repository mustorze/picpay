<?php

use App\Domains\Transaction\Models\Transaction;
use App\Domains\User\Models\Consumer;
use App\Domains\User\Models\Seller;
use Carbon\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Transaction::class, function (Faker\Generator $faker) {
    return [
        'payer_id' => function () {
            return factory(Consumer::class)->create()->id;
        },
        'payee_id' => function () {
            return factory(Seller::class)->create()->id;
        },
        'value' => rand(1, 99),
        'transaction_date' => Carbon::now()->toISOString()
    ];
});
