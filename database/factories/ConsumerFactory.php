<?php

use App\Domains\User\Models\Consumer;
use App\Domains\User\Models\User;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Consumer::class, function (Faker\Generator $faker) {
    $faker = \Faker\Factory::create('pt_BR');

    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'username' => $faker->slug
    ];
});
