<?php

use App\Domains\User\Models\User;
use Illuminate\Support\Str;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(User::class, function (Faker\Generator $faker) {
    $faker = \Faker\Factory::create('pt_BR');

    return [
        'cpf' => $faker->cpf,
        'email' => $faker->email,
        'full_name' => $faker->name,
        'password' => Str::random(8),
        'phone_number' => $faker->phoneNumber
    ];
});
