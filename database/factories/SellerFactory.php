<?php

use App\Domains\User\Models\Seller;
use App\Domains\User\Models\User;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Seller::class, function (Faker\Generator $faker) {
    $faker = \Faker\Factory::create('pt_BR');

    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'username' => $faker->slug,
        'cnpj' => $faker->cnpj,
        'fantasy_name' => $faker->name,
        'social_name' => $faker->name
    ];
});
