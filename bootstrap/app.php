<?php

require_once __DIR__ . '/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__ . '/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}
$app = new Laravel\Lumen\Application(dirname(__DIR__));

$app->withFacades();
$app->withEloquent();

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(App\Services\ErrorService::class, function () {
    return new App\Services\ErrorService;
});

$app->configure('app');
$app->configure('database');
$app->configure('logging');
$app->configure('cache');

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(Urameshibr\Providers\FormRequestServiceProvider::class);
$app->register(Illuminate\Redis\RedisServiceProvider::class);

$app->router->group(['as' => 'api', 'name' => 'api'], function ($router) {
    require __DIR__ . '/../routes/api.php';
});

return $app;
