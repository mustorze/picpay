<?php

return [
    'locale' => 'pt-BR',
    'url' => env('APP_URL')
];
