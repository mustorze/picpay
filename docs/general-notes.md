# Notas Gerais

## Sobre

### PHP
O PHP é minha linguagem favorita no momento, tenho uma alta familiaridade e me sinto preparado para desenvolver qualquer
projeto. O PHP é uma das linguagens mais utilizadas no mundo e tem uma grande comunidade trabalhando em torno dela.

### Lumen
Durante o meu aprendizado na faculdade eu estava pensando em desistir do PHP e focar em linguagens para plataformas.
Acabei comentando isso com um professor e ele me disse para procurar o Laravel. Desde a primeira impressão com o framework
vi que existia algo realmente diferente nele e a forma de como podemos aprender mais sobre, me fez repensar se realmente
iria desistir.

O Lumen é o micro-framework da família do Laravel e carrega toda sua essência.

### Boilerplate
Acabei realizando algumas modificações no boilerplate sendo-as:
- No arquivo de configuração do NGINX notei que a `$query_string` está sendo passada de maneira estranha e decidi voltar
ao padrão do Laravel
- Na imagem do PHP eu acabei instalando também a extensão do `XDebug` para conseguir gerar o relatório de cobertura do código
e também a instalação do `php-cs-fixer` para validação do estilo.
- Implantei também a imagem do `Redis` para utilização do cache.

## O Projeto
Irei falar um pouco sobre as decisões que tomei durante o desenvolvimento.

### Actions
A ideia de ações veio justamente pelo motivo de testar com mais profundidade o que uma requisição faz no sistema.
Gosto muito da ideia de `Single Action Controller` mas infelizmente fica um pouco complicado de testar com precisão.
Por isso durante o desenvolvimento desse teste resolvi mover essa responsabilidades para as Actions.

#### Como funciona
O funcionamento é bem básico e tento tirar o proveito da `injeção de dependências` do Laravel.
Pensei em alguns processos básicos que acontecem em uma requisição e as separei por métodos (poderia ter feito mais camadas
porém iria acabar deixando muito complexa a ideia).

O processo acontece da seguinte forma.
- Construtor (para injeção de dependências e dados)
- Manipulador (onde forço um caminho para a execução)
- Antes da Execução (onde posso realizar validações)
- Execução (onde de fato consumo a ação)
- Erro de execução (caso tenha acontecido algum problema no passo anterior a execução)

#### OBS
Com a ação é possível também transformá-las em JOB com a interface `ShouldQueue`, acabei não realizando pois precisava da
resposta no mesmo momento e também necessitaria adicionar mais um passo na base da ação.

## Melhorias
Me baseando na documentação da API percebi que os processos precisavam de respostas imediatas, com isso não consegui usufruir
muito dos processos em JOB. Mas que se existisse a possibilidade certamente deixaria todo o processo de Transação em fila.

## Extras
Consegui um bom valor em cobertura em testes (86%). Porém os que ficaram sem testes eram padrões do Laravel como recursos
e exceções de código. Com isso todo os processos estão testados.

Utilizei de SOLID e TDD para o desenvolvimento, além de seguir a PSR-2 e o princípio KISS.
